#include <iostream>
#include <cstring>
#include "MinimalSpanningTree.h"

using namespace std;

void printHelp(){
    cout << "On the first line, please provide number of edges of the graph.\n"
            "Next program waits for edges (if on your first line you put number 2, "
            "than program waits for two lines containing edges).\n\n"
            "The edge is given by three numbers separated by space:\n"
            "#b #e #c where:\n"
            "b = beginning node id\n"
            "e = end node id\n"
            "c = cost of the edge\n"
            "Constraints:\n"
            "- graph has to be coherent\n"
            "- if the maximum id number 8, than 9 nodes are present in graph\n"
            "- lowest number is always 0\n\n"
            "Example input:\n"
            "5 6\n"
            "0 1 2\n"
            "1 2 1\n"
            "2 3 4\n"
            "3 4 8\n"
            "4 5 9\n\n"
            "This graph contains 5 edges and 6 nodes (0,1,2,3,4,5). The highest cost is 9 (Last edge).\n"
            "For other inputs see the inputs directory.\n\n"
            "Arguments:\n"
            "--help - shows this help info\n"
            "--loop - waiting for input in loop till end is written after some of the iteration (any other input will ignore it." << endl;
}

int main(int argc, char** argv) {
    if(argc == 2 && strcmp(argv[1], "--help")==0) {
        printHelp();
    } else if(argc == 2 && strcmp(argv[1], "--loop")==0) {
        string end;
        while(true){
            cout << "Provide an input (for examples of input please use --help argument):" << endl;
            MinimalSpanningTree tree;
            tree.read();
            tree.calculate();
            tree.print();
            cout << "If you want to end program, write end. Any other input will make program continue." << endl;
            cin >> end;
            if(end == "end") break;
        }
    } else if(argc == 2) {
        throw invalid_argument("Can't recognize program argument!");
    } else {
        MinimalSpanningTree tree;
        cout << "Provide an input (for examples of input please use --help argument):" << endl;
        tree.read();
        tree.calculate();
        tree.print();
    }
    return 0;
}
