# Smestrální práce
**Author:** Marek Klement
## Zadání
Minimální kostra (minimum spanning tree)
Minimální kostry mají široké využití - od návrhů tras elektrického vedení, přes vytváření routovacích tabulek v počítačových sítích až po shlukování (clustering) bodů v analýze dat či detekce útvarů v počítačovém vidění. Doporučujeme použít Borůvkův algoritmus. Na výstupu programu bude seznam vrcholů minimální kostry a její cena nebo přímo popis celého grafu pro Graphviz, s barevně odlišenými vrcholy v kostře.

## Implementace
Implementována je pouze část s jedním vláknem. Dále je možnost zadat argument **--help**, který vypíše informace a další možnosti programu.

Program umožňuje zadat graf (zadání grafu je v sekci input) a program pak následně nalezne minimální kostru grafu pomocí Kruskalova algoritmu (Borůvkův). Nakonec vypíše jednotlivé hrany minimální kostry a celkovou cenu kostry.

Existujé také ergument **--loop**, který umožňuje opakované hledání kostry v grafu, dokud nechce uživatel skončit (napsaním slova **end**, když je k tomu vyzván).

## Input
Ukázkové vstupy lze nalézt ve složce **inputs**. Vstup vypadá následovně:
```
M N
x y c1
v w c2
...
```

Kde:
* M = Počet hran grafu, které bude uživatel zadávat
* N = Počet vrcholů grafu, které jsou řazeny od 0 až po toto číslo - 1 (pro graf o 2 vrcholech budou vrcholy - 0,1)
* x,v = Vrchol, ze kterého hrana vychází
* y,w = Vrchol, do kterého hrana vede
* c1,c2 = Cena dané hrany

Například:
```
5 6
0 1 2
1 2 1
2 3 4
3 4 8
4 5 9
```

## Output
Outputy k jednotlivým inputům lze najít ve složce **outputs**.