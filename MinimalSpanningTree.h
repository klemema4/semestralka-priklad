//
// Created by marek on 1/10/22.
//

#ifndef SHORTEST_PATH_MINIMALSPANNINGTREE_H
#define SHORTEST_PATH_MINIMALSPANNINGTREE_H

#include <limits>
#include <fstream>
#include <vector>
#include <iostream>

using namespace std;

class MinimalSpanningTree{
private:
    struct Edge {
        int from;
        int to;
        int cost;
    };

    struct Node {
        int id;
        vector<Edge> inConnections;
        vector<Edge> outConnections;
    };

    struct UndirectedNode {
        int id;
        vector<UndirectedNode> connections;
    };

    struct lessThanKey
    {
        inline bool operator() (const Edge& e1, const Edge& e2)
        {
            return (e1.cost < e2.cost);
        }
    };

    struct Graph {
        vector<Edge> edges;
        vector<Node> nodes;
    };

    bool isRead = false, isCalculate = false;
    Graph graph;
    vector<Edge> spanningTreeEdges;
    vector<UndirectedNode> nodesConnections;
public:
    MinimalSpanningTree() = default;
    void read();
    void calculate();
    void print();
    bool checkLoop();
    bool loopUtil(int v, bool *visited, int parent);
};


#endif //SHORTEST_PATH_MINIMALSPANNINGTREE_H
