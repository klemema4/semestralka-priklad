//
// Created by marek on 1/10/22.
//

#include <algorithm>
#include "MinimalSpanningTree.h"

inline void readNumber(int *x) {

    // faster reading
    register char c = getchar_unlocked();
    *x = 0;
    for (; (c < 48) || (c > 57); c = getchar_unlocked());
    for (; (c > 47) && (c < 58); c = getchar_unlocked()) {
        *x = (int) ((((*x) << 1) + ((*x) << 3)) + c - 48);
    }
}

void MinimalSpanningTree::read() {
    int edgesNumber;
    int nodesNumber;
    readNumber(& edgesNumber);
    readNumber(& nodesNumber);
    vector<Edge> allEdges;
    vector<Node> allNodes;
    bool connected[nodesNumber];
    for(int i = 0; i < nodesNumber; i++){
        connected[i] = false;
    }

    int from, to, cost;
    for (int i = 0; i < edgesNumber; ++i) {
        readNumber(& from);
        readNumber(& to);
        readNumber(& cost);
        connected[from] = true;
        connected[to] = true;

        Edge edge = {.from = from, .to = to, .cost = cost};
        allEdges.push_back(edge);
    }
    for(int i = 0; i < nodesNumber; i++){
        if(!connected[i]){
            throw invalid_argument( &"One of nodes is not connected in graph - #node = " [ i]);
        }
    }
    for(int i = 0; i < nodesNumber; ++i){
        Node node = {.id = i};
        allNodes.push_back(node);
    }
    for(auto edge : allEdges){
        allNodes.at(edge.from).outConnections.push_back(edge);
        allNodes.at(edge.to).inConnections.push_back(edge);
    }
    this->graph = {.edges = allEdges, .nodes = allNodes};
    this->isRead = true;
}

bool MinimalSpanningTree::loopUtil(int nodeId, bool visited[], int parent){
    visited[nodeId] = true;
    UndirectedNode nd = nodesConnections.at(nodeId);
    for (const auto& next : nd.connections){
        if (!visited[next.id]){
            if (loopUtil(next.id, visited, nodeId))
                return true;
        }
        else if (next.id != parent)
            return true;
    }
    return false;
}

bool MinimalSpanningTree::checkLoop(){
    int size = graph.nodes.size();
    bool *visited = new bool[size];
    for (int i = 0; i < size; i++)
        visited[i] = false;

    for (int i = 0; i < size; i++){
        if (!visited[i])
            if (loopUtil(i, visited, -1))
                return true;
    }
    return false;
}

void MinimalSpanningTree::calculate() {
    sort(graph.edges.begin(), graph.edges.end(), lessThanKey());
    for(int i = 0; i < graph.nodes.size(); i++){
        UndirectedNode nd = {.id = i};
        nodesConnections.push_back(nd);
    }
    int nodesCount = 0;
    for (auto edge : graph.edges) {
        if(nodesCount == graph.nodes.size()) break;
        nodesConnections.at(edge.from).connections.push_back(nodesConnections.at(edge.to));
        nodesConnections.at(edge.to).connections.push_back(nodesConnections.at(edge.from));
        if(!checkLoop()){
            nodesCount++;
            spanningTreeEdges.push_back(edge);
        } else {
            nodesConnections.at(edge.from).connections.pop_back();
            nodesConnections.at(edge.to).connections.pop_back();
        }
    }
    if(spanningTreeEdges.size() + 1 != graph.nodes.size()){
        throw invalid_argument( "Minimal spanning tree computation did not finish successfully. Please see input graph." );
    }
    isCalculate = true;

}

void MinimalSpanningTree::print() {
    cout << "Minimal spanning tree:" << endl;
    int totalCost = 0;
    for(auto edge : spanningTreeEdges){
        totalCost += edge.cost;
        cout << edge.from << " " << edge.to << " Cost - " << edge.cost << endl;
    }
    cout << "Minimal spanning tree cost: " << totalCost << endl;
}
